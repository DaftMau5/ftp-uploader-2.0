package com.eduardocarvalho.FTPuploader.main;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;


/*
 * 13/12/2017 - S�o Paulo, Brasil
 * 
 * by Eduardo Carvalho
 * 
 * https://bitbucket.org/DaftMau5
 */
public class Upload {

	public static void main(String[] args) {
		// vari�veis de conex�o ao FTP
		String server = "";
		String user = "";
		String pass = "";
		String port = "";
		// tempo de sleep da thread
		String tempo = "";
		
		
		File dir = new File("");
		
		// atribuindo valores �s vari�veis de conex�o do arquivo properties
		try {
			Properties prop = getProp();
			server = prop.getProperty("server");
			user = prop.getProperty("user");
			pass = prop.getProperty("senha");
			port = prop.getProperty("porta");
			tempo = prop.getProperty("tempo");
			dir = new File(prop.getProperty("dir"));
		} catch (Exception e1) {
			System.out.println("Erro com arquivo properties - "+e1);
		}

		File uploadfile = new File("");
		System.out.println("server: "+server+" user: "+user+" pass: "+pass+" port: "+port+" dir: "+dir);
		
		
		while (true) {
			// Pattern r = Pattern.compile("(_[0-9]{1}.txt)$");
			boolean f;
			File[] lista = dir.listFiles();
			
			for (File file : lista) {
				if (file.isDirectory()) {
					continue;
				}
				
				if (file.getName().endsWith(".txt")) {
					uploadfile = file;
					System.out.println(file.getName() + " encontrado...");
				}

				FTPClient ftpClient = new FTPClient();

				try {
					try {
						ftpClient.connect(server, Integer.parseInt(port));
						ftpClient.login(user, pass);
						ftpClient.enterLocalPassiveMode();
						ftpClient.setFileType(FTP.BINARY_FILE_TYPE);

					} catch (Exception e) {
						System.out.println("Erro de conex�o - " + e);
					}

					try {
						String remoteFile = uploadfile.getName();
						InputStream outputStream = new FileInputStream(uploadfile);

						int i = 0;
						boolean success = false;

						while (true) {
							success = ftpClient.storeFile(remoteFile, outputStream);
							i++;

							if (success) {
								System.out.println("Enviado com sucesso!");
								f = true;
								break;
							} else if (i > 2) {
								f = false;
								System.out.println("O envio falhou ap�s 3 tentativas!");
							} else {
								f = false;
								System.out.println("O envio falhou!");
							}
						}

						outputStream.close();
					} catch (Exception e) {
						f = false;
						System.out.println("Erro com arquivo - " + e);
					}

				} catch (Exception e) {
					f = false;
					System.out.println("ERRO! - " + e.getMessage());
				} finally {
					try {
						ftpClient.disconnect();
					} catch (Exception e2) {
						System.out.println("Erro em disconnect - " + e2.getMessage());
					}
				}
				
				try {
					if (f) {
						File novo = new File(dir + "/enviado/" + uploadfile.getName());
						boolean a = uploadfile.renameTo(novo);
						if (a) {
							System.out.println("Arquivo movido para enviados.");
						} else {
							System.out.println("Arquivo nao movido para enviados.");
						}
					} else {
						System.out.println("Arquivo nao movido para enviados.");
					}
				} catch (Exception e) {
					System.out.println("Arquivo nao movido para enviados - " + e);
				}
				System.out.println("--------------");
			}
			
			try {
				System.out.println("Dormindo...");
				System.out.println("--------------");
				Thread.sleep(Integer.parseInt(tempo));
			} catch (Exception e) {
				System.out.println("ERRO - " + e);
			}
		}
	}
	public static Properties getProp() throws IOException {
		Properties props = new Properties();
		FileInputStream file = new FileInputStream(
				"c:/testeFTP/properties/properties.txt");
		props.load(file);
		return props;
	}
}
